<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>MovieCha</title>
</head>
<body>
<!--Navigation du site -->
<div class="container site">
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container-fluid">
            <h1 class="navbar-brand text-logo" href="index.php">MovieTest</h1>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0 ">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="film.php">Film</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="contact.php">Contacts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " aria-current="page" href="#2">A propos</a>
                    </li>
                </ul>

            </div>
        </div>
    </nav>
