<?php

try
{
    $db = new PDO("mysql:host=localhost;dbname=movietest;charset=utf8", "root","");

}
catch(PDOException $e)
{
    die("Error" . $e->getMessage());
}
$film = intval($_GET['q']);
$requete = $db->prepare("SELECT film.titre AS titreF,YEAR(film.annee) AS anneeF,metier.nom AS nomM,contact.prenom AS prenomC
                                    ,contact.nom AS nomC FROM concerner, film,metier,contact 
                                    WHERE concerner.id = ? AND concerner.idFilm=film.id AND concerner.idContact =contact.id AND concerner.idMetier =metier.id ");
$requete->execute(array($film));
var_dump($requete);
?>

    <section class="row tab-content">
        <h1>Liste des Film</h1>
        <table class="table table-hover tab-content text-center">

            <thead class="table-dark ">
            <tr>
            <th>Titre Film</th>
            <th>Année Film</th>
            <th>Metier avec Contact</th>
            </tr>
            </thead>
            <tbody>
            <?php while($resultat = $requete->fetch()){ ?>
                <tr>
                    <td><?php echo $resultat["titreF"]; ?></td>
                    <td><?php echo $resultat["anneeF"]; ?></td>
                    <td><?php echo $resultat["nomM"] .": ".$resultat["prenomC"]."   " .$resultat["nomC"] ; ?></td>
                </tr>
            <?php } ?>
            </tbody>

        </table>
    </section>
<?php include 'HeadFoot/footer.php';
