CREATE TABLE Film(
                     id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                     titre varchar (255) NOT NULL,
                     annee DATE NOT NULL
);

CREATE TABLE Contact (
                         id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                         civilite ENUM('Madame','Monsieur'),
                         prenom varchar (10) NOT NULL,
                         nom varchar (255) NOT NULL
);

CREATE TABLE Metier (
                        id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        nom varchar (255) NOT NULL
);
CREATE TABLE Concerner (
                        id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                        idContact int(11) NOT NULL,
                        idFilm int(11) NOT NULL,
                        idMetier int(11) NOT NULL

);
ALTER TABLE `concerner` ADD FOREIGN KEY (`idContact`) REFERENCES `contact`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `concerner` ADD FOREIGN KEY (`idFilm`) REFERENCES `film`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `concerner` ADD FOREIGN KEY (`idMetier`) REFERENCES `metier`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;