<div class="row tab-content">
    <h1 class="mb-3">Ajouter un Item</h1>
    <form method="post" action="insert.php" enctype="multipart/form-data" role="form">
        <div class="mb-3">
            <label for="name"> Name</label>
            <input type="text" class="form-control" id="name" name="name" value="<?php echo $name;  ?>">
            <span class="help-inline"><?php echo $nameError;?></span>
        </div>
        <div class="mb-3">
            <label for="desc"> Description</label>
            <input type="text" class="form-control" id="desc" name="description" value="<?php echo $desc;  ?>">
            <span class="help-inline"><?php echo $descError;?></span>
        </div>
        <div class="mb-3">
            <label for="price"> Price</label>
            <input type="text" class="form-control" id="price" name="price" value="<?php echo $price;  ?>">
            <span class="help-inline"><?php echo $priceError;?></span>
        </div>

        <select class="form-select mb-3" name ="category">
            <?php
            $db= database::Connect();
            foreach (($db->query("SELECT * FROM category")) as $category){
                echo '<option value="'.$category["id"].'">' .$category["nameCategory"]."</option>";
            }
            database::discConnect();
            ?>
            <span class="help-inline"><?php echo $cateError;?></span>
        </select>

        <div class="mb-3">
            <label for="image"> Selectionnez une image</label>
            <input type="file" class="form-control" id="image" name=" image" value="<?php echo $image;  ?>">
            <span class="help-inline"><?php echo $imageError;?></span>
        </div>
        <button type="submit"  class="btn btn-outline-primary">Add Item</button>
        <a href="index.php"  class="btn btn-outline-danger">return </a>
    </form>
</div>
